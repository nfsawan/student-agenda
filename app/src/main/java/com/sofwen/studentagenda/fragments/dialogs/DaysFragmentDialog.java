package com.sofwen.studentagenda.fragments.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sofwen.studentagenda.R;

import org.w3c.dom.Text;

public class DaysFragmentDialog extends DialogFragment {
    private View customView;
    private String title="";

    public void setTitle(String title) {
        this.title = title;
    }

    private View.OnClickListener btnCancelClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };

    private View.OnClickListener btnSelectClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };

    public void setBtnCancelClickListner(View.OnClickListener btnCancelClickListner) {
        this.btnCancelClickListner = btnCancelClickListner;
    }

    public void setBtnSelectClickListner(View.OnClickListener btnSelectClickListner) {
        this.btnSelectClickListner = btnSelectClickListner;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        customView = getActivity().getLayoutInflater().inflate(
                R.layout.days_dialog_fragment, null);

        Dialog dialog=new Dialog(getContext(),R.style.Base_Theme_AppCompat_Dialog);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        Button btnCancel=(Button)customView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelClickListner);

        Button btnMonday=(Button)customView.findViewById(R.id.btnMonday);
        btnMonday.setOnClickListener(btnSelectClickListner);

        Button btnTuesday=(Button)customView.findViewById(R.id.btnTuesday);
        btnTuesday.setOnClickListener(btnSelectClickListner);

        Button btnWednesday=(Button)customView.findViewById(R.id.btnWednesday);
        btnWednesday.setOnClickListener(btnSelectClickListner);

        Button btnThursday=(Button)customView.findViewById(R.id.btnThursday);
        btnThursday.setOnClickListener(btnSelectClickListner);

        Button btnFriday=(Button)customView.findViewById(R.id.btnFriday);
        btnFriday.setOnClickListener(btnSelectClickListner);

        Button btnSaturday=(Button)customView.findViewById(R.id.btnSaturday);
        btnSaturday.setOnClickListener(btnSelectClickListner);

        Button btnSunday=(Button)customView.findViewById(R.id.btnSunday);
        btnSunday.setOnClickListener(btnSelectClickListner);

        if(title.equals("Select first day of week"))
        {
            TextView tvTitle=(TextView)customView.findViewById(R.id.tvTitle);
            tvTitle.setText(title);
            btnTuesday.setVisibility(View.GONE);
            btnWednesday.setVisibility(View.GONE);
            btnThursday.setVisibility(View.GONE);
            btnFriday.setVisibility(View.GONE);
            btnSaturday.setVisibility(View.GONE);
        }

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);

        return dialog;
    }


}
