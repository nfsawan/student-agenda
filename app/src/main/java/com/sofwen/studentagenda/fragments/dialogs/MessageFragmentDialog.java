package com.sofwen.studentagenda.fragments.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sofwen.studentagenda.R;

public class MessageFragmentDialog extends DialogFragment {
    private View customView;
    private String title="";
    private String message="";
    private View.OnClickListener btnNoClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };

    private View.OnClickListener btnYesClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };

    public void setBtnNoClickListner(View.OnClickListener btnNoClickListner) {
        this.btnNoClickListner = btnNoClickListner;
    }

    public void setBtnYesClickListner(View.OnClickListener btnYesClickListner) {
        this.btnYesClickListner = btnYesClickListner;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        customView = getActivity().getLayoutInflater().inflate(
                R.layout.message_fragment_dialog, null);

        Dialog dialog=new Dialog(getContext(),R.style.Base_Theme_AppCompat_Dialog);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        Button btnNo=(Button)customView.findViewById(R.id.btnNo);
        btnNo.setOnClickListener(btnNoClickListner);
        Button btnYes=(Button)customView.findViewById(R.id.btnYes);
        btnYes.setOnClickListener(btnYesClickListner);
        TextView tvTitle=(TextView)customView.findViewById(R.id.tvTitle);
        TextView tvMessage=(TextView)customView.findViewById(R.id.tvMessage);
        tvTitle.setText(title);
        tvMessage.setText(message);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);

        return dialog;
    }


}
