package com.sofwen.studentagenda.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;

/**
 * Created by zain on 10/19/2015.
 */

public class EventsDialogFragment extends DialogFragment {
    View v;
    Dialog dialog;
    private Button btnExam;
    private Button btnCourseWork;
    private Button btnReturnBook;
    private Button btnCancel;
    private Button btnActivity;
    String eventText;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        dialog = new AlertDialog.Builder(getActivity(),R.style.Base_Theme_AppCompat_Dialog)
//                .setView(customView).setCancelable(false).create();
        v = getActivity().getLayoutInflater().inflate(R.layout.events_dialog_fragment, null);
        dialog = new AlertDialog.Builder(getActivity(), R.style.Base_Theme_AppCompat_Dialog).setView(v).setCancelable(false).create();
        btnExam = (Button) v.findViewById(R.id.btnExam);
        btnCourseWork = (Button) v.findViewById(R.id.btnCourseWork);
        btnReturnBook = (Button) v.findViewById(R.id.btnReturnBook);
        btnActivity = (Button) v.findViewById(R.id.btnActivity);
        btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnExam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView= (TextView) v;
                eventText= textView.getText().toString();
                AddEventsFragment addEventsFragment=AddEventsFragment.createFragment();
                addEventsFragment.setEventText(eventText);
                CommonMethods.callFragment(addEventsFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), AddSubjectFragment.TAG);
                dialog.dismiss();

            }
        });
        btnCourseWork.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView= (TextView) v;
                eventText= textView.getText().toString();
                AddEventsFragment addEventsFragment=AddEventsFragment.createFragment();
                addEventsFragment.setEventText(eventText);
                CommonMethods.callFragment(addEventsFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), AddSubjectFragment.TAG);
                dialog.dismiss();
            }
        });
        btnReturnBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView= (TextView) v;
                eventText= textView.getText().toString();
                AddEventsFragment addEventsFragment=AddEventsFragment.createFragment();
                addEventsFragment.setEventText(eventText);
                CommonMethods.callFragment(addEventsFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), AddSubjectFragment.TAG);
                dialog.dismiss();
            }
        });
        btnActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView textView= (TextView) v;
                eventText= textView.getText().toString();
                AddEventsFragment addEventsFragment=AddEventsFragment.createFragment();
                addEventsFragment.setEventText(eventText);
                CommonMethods.callFragment(addEventsFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), AddSubjectFragment.TAG);
                dialog.dismiss();
            }
        });
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }
}
