package com.sofwen.studentagenda.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.acitvities.MainActivity;
import com.sofwen.studentagenda.adapters.ListItemTimetableAdapter;
import com.sofwen.studentagenda.business.objects.Color;

import java.util.ArrayList;

import petrov.kristiyan.colorpicker.ColorPicker;

public class ColorFragment extends Fragment {
    public static final String TAG = ColorFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private ImageView ivMenuBackgroundColor;
    private ImageView ivExamColor;
    private ImageView ivActivityColor;
    private ImageView ivCourseworkOrHomeworkColor;
    private ImageView ivReturnBookColor;
    private ArrayList<Color> colors=new ArrayList<>();
    private MainActivity.OnUpdateNotifier onUpdateNotifier;

    public void setOnUpdateNotifier(MainActivity.OnUpdateNotifier onUpdateNotifier) {
        this.onUpdateNotifier = onUpdateNotifier;
    }

    public static ColorFragment createFragment() {
        ColorFragment addSubjectFragment = new ColorFragment();
        return addSubjectFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.color_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("Colors");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Color color = colors.get(0);
                    color.setBackgroundColor(((ColorDrawable) ivMenuBackgroundColor.getBackground()).getColor());
                    color.setExamColor(((ColorDrawable) ivExamColor.getBackground()).getColor());
                    color.setActivityColor(((ColorDrawable) ivActivityColor.getBackground()).getColor());
                    color.setCourseworkHomeworkColor(((ColorDrawable) ivCourseworkOrHomeworkColor.getBackground()).getColor());
                    color.setReturnBookColor(((ColorDrawable) ivReturnBookColor.getBackground()).getColor());
                    CommonObjects.getDbHelper().updateColor(color);
                    getFragmentManager().popBackStack();
                    onUpdateNotifier.onUpdate();
                    CommonMethods.showToast(Messages.APP_COLORS_UPDATED_SUCCESSFULLY, Toast.LENGTH_LONG);
                } catch (Exception e) {
                    CommonMethods.showToast(Messages.FAILED_TO_UPDATE_APP_COLORS, Toast.LENGTH_LONG);
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        ivMenuBackgroundColor=(ImageView)v.findViewById(R.id.ivMenuBackgroundColor);
        ivMenuBackgroundColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivMenuBackgroundColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        ivExamColor=(ImageView)v.findViewById(R.id.ivExamColor);
        ivExamColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivExamColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        ivActivityColor=(ImageView)v.findViewById(R.id.ivActivityColor);
        ivActivityColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivActivityColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        ivCourseworkOrHomeworkColor=(ImageView)v.findViewById(R.id.ivCourseworkOrHomeworkColor);
        ivCourseworkOrHomeworkColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivCourseworkOrHomeworkColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        ivReturnBookColor=(ImageView)v.findViewById(R.id.ivReturnBookColor);
        ivReturnBookColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivReturnBookColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        addDefaultColor();
        try {
            colors=CommonObjects.getDbHelper().getColors();
            if(colors.size()!=0)
            {
                ivMenuBackgroundColor.setBackgroundColor(colors.get(0).getBackgroundColor());
                ivExamColor.setBackgroundColor(colors.get(0).getExamColor());
                ivActivityColor.setBackgroundColor(colors.get(0).getActivityColor());
                ivCourseworkOrHomeworkColor.setBackgroundColor(colors.get(0).getCourseworkHomeworkColor());
                ivReturnBookColor.setBackgroundColor(colors.get(0).getReturnBookColor());
            }
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_APP_COLORS, Toast.LENGTH_LONG);
        }
    }

    private void addDefaultColor()
    {
        try {
            if (CommonObjects.getDbHelper().getColors().size() == 0)
            {
                Color color=new Color();
                color.setBackgroundColor(-11419154);
                color.setExamColor(-4224594);
                color.setActivityColor(-740056);
                color.setCourseworkHomeworkColor(-10965321);
                color.setReturnBookColor(-14654801);
                CommonObjects.getDbHelper().insertColor(color);
            }
        } catch (Exception e) {
        }
    }

}
