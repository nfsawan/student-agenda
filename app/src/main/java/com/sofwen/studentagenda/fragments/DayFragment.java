package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.adapters.ListItemSubjectTimetableByDayAdapter;
import com.sofwen.studentagenda.adapters.ListItemTimetableAdapter;
import com.sofwen.studentagenda.business.objects.Timetable;
import com.sofwen.studentagenda.fragments.dialogs.MessageFragmentDialog;
import com.sofwen.studentagenda.fragments.dialogs.SelectEditorFragmentDialog;

import java.util.ArrayList;

public class DayFragment extends Fragment {
    private TextView tvTitle;
    private ImageButton ibLeft;
    private ImageButton ibRight;
    private int dateCount=0;
    private ListView listView;
    private ArrayList<Timetable> timetables=new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.day_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        ibLeft=(ImageButton)v.findViewById(R.id.btnLeft);
        ibRight=(ImageButton)v.findViewById(R.id.btnRight);
        ibLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount--;
                tvTitle.setText(CommonMethods.getDateByCount(dateCount));
                updateViewByDay();
            }
        });
        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount++;
                tvTitle.setText(CommonMethods.getDateByCount(dateCount));
                updateViewByDay();
            }
        });
        tvTitle.setText(CommonMethods.getDateByCount(dateCount));
        listView=(ListView)v.findViewById(R.id.listView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Timetable timetable=(Timetable) parent.getAdapter().getItem(position);
                final SelectEditorFragmentDialog selectEditorFragmentDialog=new SelectEditorFragmentDialog();
                selectEditorFragmentDialog.setTimetable(timetable);
                selectEditorFragmentDialog.setBtnDeleteClickListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectEditorFragmentDialog.dismiss();
                        final MessageFragmentDialog messageFragmentDialog = new MessageFragmentDialog();
                        messageFragmentDialog.setTitle("Delete Alert!");
                        messageFragmentDialog.setMessage("Are you sure you want to delete this item");
                        messageFragmentDialog.setBtnYesClickListner(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                messageFragmentDialog.dismiss();
                                try {
                                    messageFragmentDialog.dismiss();
                                    CommonObjects.getDbHelper().deleteTimetableBySubjectId(timetable.getId());
                                    CommonMethods.showToast(Messages.TIMETABLE_DELETED_SUCCESSFULLY, Toast.LENGTH_LONG);
                                    timetables = updateList(timetables, timetable);
                                    listView.setAdapter(new ListItemSubjectTimetableByDayAdapter(getContext(), getListByDayElements(timetables),R.layout.list_item_subject_timetable_by_day));
                                } catch (Exception e) {
                                    CommonMethods.showToast(Messages.FAILED_TO_DELETE_TIMETABLE, Toast.LENGTH_LONG);
                                }
                            }
                        });
                        messageFragmentDialog.show(getActivity().getSupportFragmentManager(), "");
                    }
                });
                selectEditorFragmentDialog.show(getActivity().getSupportFragmentManager(),"");
            }
        });
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if(menuVisible)
        {
            updateViewByDay();
        }
    }

    private ArrayList<Timetable> getListByDayElements(ArrayList<Timetable> items) {
        String date[]=tvTitle.getText().toString().split(",");
        String day=date[0];
        ArrayList<Timetable> selectiveItems = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getDay().equals(day)) {
                selectiveItems.add(items.get(i));
            }
        }
        return selectiveItems;
    }

    private void updateViewByDay()
    {
        try {
            if (listView != null) {
                timetables = CommonObjects.getDbHelper().getTimetables();
                listView.setAdapter(new ListItemSubjectTimetableByDayAdapter(getContext(), getListByDayElements(timetables), R.layout.list_item_subject_timetable_by_day));
            }
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_TIMETABLE, Toast.LENGTH_LONG);
        }
    }

    private ArrayList<Timetable> updateList(ArrayList<Timetable> items,Timetable timetable)
    {
        ArrayList<Timetable> selectiveItems=new ArrayList<>();
        for(int i=0;i<items.size();i++)
        {
            if(items.get(i).getId()!=timetable.getId())
            {
                selectiveItems.add(items.get(i));
            }
        }
        return selectiveItems;
    }

}
