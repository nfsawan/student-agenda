package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Event;

import java.util.Calendar;

public class UpdateEventsFragment extends Fragment {
    public static final String TAG = UpdateEventsFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private EditText etName;
    private TextView tvEventText;
    private CheckBox cbAddReminder;
    String eventText;
    private LinearLayout llReminderDate;
    private LinearLayout llReminderTime;
    private TextView tvDate;
    private TextView tvTime;
    private EditText etDescription;
    private TextView etReminderDate;
    private TextView etReminderTime;
    private int day,month,year,hour,minut;
    private Event event;
    private EventTypeDialogFragment dialog;

    public void setEvent(Event event) {
        this.event = event;
    }

    public static UpdateEventsFragment createFragment() {
        return new UpdateEventsFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.add_events_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        getCurrentDateTime();
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("Update Events");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        cbAddReminder= (CheckBox) v.findViewById(R.id.cbAddReminder);
        llReminderDate= (LinearLayout) v.findViewById(R.id.llReminderDate);
        llReminderTime= (LinearLayout) v.findViewById(R.id.llReminderTime);
        etDescription= (EditText) v.findViewById(R.id.etDescription);
        tvDate= (TextView) v.findViewById(R.id.tvDate);
        etReminderDate= (TextView) v.findViewById(R.id.etReminderDate);
        etReminderTime= (TextView) v.findViewById(R.id.etReminderTime);
        etReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callDatePicker(getActivity(), "", reminderDatePickerListener, getActivity().getSupportFragmentManager());
            }
        });
        etReminderTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(),"",onReminderTimeSetListener,getActivity().getSupportFragmentManager());

            }
        });
//        tvDate.setText(Calend);
        tvTime= (TextView) v.findViewById(R.id.tvTime);
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(), "", onTimeSetListener, getActivity().getSupportFragmentManager());
            }
        });
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callDatePicker(getActivity(), "", datePickerListener, getActivity().getSupportFragmentManager());
            }
        });

        cbAddReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    llReminderDate.setVisibility(View.VISIBLE);
                    llReminderTime.setVisibility(View.VISIBLE);
                    etReminderDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
                    etReminderTime.setText(hour + ":" + minut + "");
                } else {
                    llReminderDate.setVisibility(View.GONE);
                    llReminderTime.setVisibility(View.GONE);
                }
            }
        });
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etName.getText().toString().equals("")) {
                    CommonMethods.showToast(Messages.EVENT_NAME_REQUIRED, Toast.LENGTH_LONG);
                } else {
                    event.setName(etName.getText().toString());
                    event.setType(eventText);
                    event.setDate(tvDate.getText().toString());
                    event.setTime(tvTime.getText().toString());
                    Log.d("**TImeSendingToDB", tvTime.getText().toString());
                    event.setDescription(etDescription.getText().toString());
                    event.setSubjectId(0);
                    if (cbAddReminder.isChecked()) {
                        event.setReminderId("1000");
                        event.setReminderDate(etReminderDate.getText().toString());
                        event.setReminderTime(etReminderTime.getText().toString());
                    } else {
                        event.setReminderId("");
                        event.setReminderDate("");
                        event.setReminderTime("");
                    }
//                    subject.setYear(etYear.getText().toString());
                    try {
                        CommonObjects.getDbHelper().updateEvent(event);
                        getFragmentManager().popBackStack();
                        CommonMethods.showToast(Messages.EVENT_ADDED_SUCCESSFULLY, Toast.LENGTH_LONG);
                    } catch (Exception e) {
                        CommonMethods.showToast(Messages.FAILED_TO_ADD_EVENT, Toast.LENGTH_LONG);
                    }
                }
            }
        });
            v.setFocusableInTouchMode(true);
        v.requestFocus();
            v.setOnKeyListener(new View.OnKeyListener()

            {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == KeyEvent.ACTION_DOWN) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            getFragmentManager().popBackStack();
                            return true;
                        }
                    }
                    return false;
                }
            });
        etName=(EditText)v.findViewById(R.id.etName);
        tvEventText = (TextView) v.findViewById(R.id.tvEventText);
        tvEventText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = UpdateEventsFragment.this.getFragmentManager();
                dialog = new EventTypeDialogFragment();
                dialog.show(fm, "Events Type Dialog");
                dialog.setBtnExamListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (TextView) v;
                        tvEventText.setText(textView.getText().toString());
                        eventText = textView.getText().toString();
                        dialog.dismiss();
                    }
                });
                dialog.setBtnCourseWorkListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (TextView) v;
                        tvEventText.setText(textView.getText().toString());
                        eventText=textView.getText().toString();
                        dialog.dismiss();
                    }
                });
                dialog.setBtnActivityListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (TextView) v;
                        tvEventText.setText(textView.getText().toString());
                        eventText=textView.getText().toString();
                        dialog.dismiss();
                    }
                });
                dialog.setBtnReturnBookListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextView textView = (TextView) v;
                        tvEventText.setText(textView.getText().toString());
                        eventText=textView.getText().toString();
                        dialog.dismiss();
                    }
                });
            }
        });
        tvDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
            CommonMethods.setupUI(v,getContext()

            );
            etName.setText(event.getName());
            tvDate.setText(event.getDate());
            tvTime.setText(event.getTime());
        etDescription.setText(event.getDescription());
        tvEventText.setText(event.getType());
        if(event.getReminderDate().equals("")){
                cbAddReminder.setChecked(false);
            llReminderDate.setVisibility(View.GONE);
            llReminderTime.setVisibility(View.GONE);
        }  else {
            cbAddReminder.setChecked(true);
                llReminderDate.setVisibility(View.VISIBLE);
                llReminderTime.setVisibility(View.VISIBLE);
                etReminderDate.setText(event.getReminderDate());
            etReminderTime.setText(event.getReminderTime());
        }

    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year,
                              int month, int day) {
         tvDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
        }
    };
    private DatePickerDialog.OnDateSetListener reminderDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year,
                              int month, int day) {
            etReminderDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
        }
    };
    private TimePickerDialog.OnTimeSetListener onTimeSetListener= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i1) {

            tvTime.setText(i + ":"+ i1 +"");
        }
    };
    private TimePickerDialog.OnTimeSetListener onReminderTimeSetListener= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i1) {

            etReminderTime.setText(i + ":"+ i1 +"");
        }
    };
    private void getCurrentDateTime(){
        Calendar calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minut = calendar.get(Calendar.MINUTE);
    }

//    @Override
//    public void onResume() {
//        tvEventText.setText(eventText);
//        super.onResume();
//    }
}
