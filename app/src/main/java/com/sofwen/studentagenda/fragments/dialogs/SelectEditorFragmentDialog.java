package com.sofwen.studentagenda.fragments.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.business.objects.Timetable;
import com.sofwen.studentagenda.fragments.AddTimetableFragment;
import com.sofwen.studentagenda.fragments.UpdateTimetableFragment;

public class SelectEditorFragmentDialog extends DialogFragment {
    private View customView;
    private Timetable timetable;

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }



    private View.OnClickListener btnCancelClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };

    private View.OnClickListener btnCopyClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AddTimetableFragment addTimetableFragment=new AddTimetableFragment();
            addTimetableFragment.setTimetable(timetable);
            CommonMethods.callFragment(addTimetableFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), AddTimetableFragment.TAG);
            dismiss();
        }
    };

    private View.OnClickListener btnEditClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            UpdateTimetableFragment updateTimetableFragment = UpdateTimetableFragment.createFragment();
            updateTimetableFragment.setTimetable(timetable);
            CommonMethods.callFragment(updateTimetableFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), UpdateTimetableFragment.TAG);
            dismiss();
        }
    };

    private View.OnClickListener btnDeleteClickListner=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    public void setBtnCancelClickListner(View.OnClickListener btnCancelClickListner) {
        this.btnCancelClickListner = btnCancelClickListner;
    }

    public void setBtnCopyClickListner(View.OnClickListener btnCopyClickListner) {
        this.btnCopyClickListner = btnCopyClickListner;
    }

    public void setBtnDeleteClickListner(View.OnClickListener btnDeleteClickListner) {
        this.btnDeleteClickListner = btnDeleteClickListner;
    }

    public void setBtnEditClickListner(View.OnClickListener btnEditClickListner) {
        this.btnEditClickListner = btnEditClickListner;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        customView = getActivity().getLayoutInflater().inflate(
                R.layout.select_editor_fragment_dialog, null);

        Dialog dialog=new Dialog(getContext(),R.style.Base_Theme_AppCompat_Dialog);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        Button btnCancel=(Button)customView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelClickListner);

        Button btnCopy=(Button)customView.findViewById(R.id.btnCopy);
        btnCopy.setOnClickListener(btnCopyClickListner);

        Button btnEdit=(Button)customView.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(btnEditClickListner);

        Button btnDelete=(Button)customView.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(btnDeleteClickListner);

        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);

        return dialog;
    }



}
