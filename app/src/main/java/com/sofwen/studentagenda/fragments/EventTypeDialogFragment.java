package com.sofwen.studentagenda.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;

/**
 * Created by zain on 10/19/2015.
 */

public class EventTypeDialogFragment extends DialogFragment {
    View v;
    Dialog dialog;
    private Button btnExam;
    private Button btnCourseWork;
    private Button btnReturnBook;
    private Button btnCancel;
    private Button btnActivity;
    public View.OnClickListener btnExamListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };
    public View.OnClickListener btnCourseWorkListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };
    public View.OnClickListener btnActivityListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };
    public View.OnClickListener btnReturnBookListener=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            dismiss();
        }
    };

    public void setBtnExamListener(View.OnClickListener btnExamListener) {
        this.btnExamListener = btnExamListener;
    }

    public void setBtnCourseWorkListener(View.OnClickListener btnCourseWorkListener) {
        this.btnCourseWorkListener = btnCourseWorkListener;
    }

    public void setBtnActivityListener(View.OnClickListener btnActivityListener) {
        this.btnActivityListener = btnActivityListener;
    }

    public void setBtnReturnBookListener(View.OnClickListener btnReturnBookListener) {
        this.btnReturnBookListener = btnReturnBookListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {
        v = getActivity().getLayoutInflater().inflate(R.layout.events_dialog_fragment, null);
        dialog = new AlertDialog.Builder(getActivity(), R.style.Base_Theme_AppCompat_Dialog).setView(v).setCancelable(false).create();
        btnExam = (Button) v.findViewById(R.id.btnExam);
        btnExam.setOnClickListener(btnExamListener);
        btnCourseWork = (Button) v.findViewById(R.id.btnCourseWork);
        btnCourseWork.setOnClickListener(btnCourseWorkListener);
        btnReturnBook = (Button) v.findViewById(R.id.btnReturnBook);
        btnReturnBook.setOnClickListener(btnReturnBookListener);
        btnActivity = (Button) v.findViewById(R.id.btnActivity);
        btnActivity.setOnClickListener(btnActivityListener);
        btnCancel = (Button) v.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        return dialog;
    }
}
