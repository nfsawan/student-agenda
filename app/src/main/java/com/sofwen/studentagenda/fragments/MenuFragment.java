package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.acitvities.MainActivity;
import com.sofwen.studentagenda.business.objects.Color;

public class MenuFragment extends Fragment implements View.OnClickListener {

    private ImageButton ibSubjects;
    private ImageButton ibTimeTable;
    private ImageButton ibEvents;
    private ImageButton ibMarks;
    private ImageButton ibSettings;
    private ImageButton ibColors;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.menu_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        ibSubjects = (ImageButton) v.findViewById(R.id.ibSubjects);
        ibTimeTable = (ImageButton) v.findViewById(R.id.ibTimeTable);
        ibEvents = (ImageButton) v.findViewById(R.id.ibEvents);
        ibMarks = (ImageButton) v.findViewById(R.id.ibMarks);
        ibSettings = (ImageButton) v.findViewById(R.id.ibSettings);
        ibColors = (ImageButton) v.findViewById(R.id.ibColors);
        ibSubjects.setOnClickListener(this);
        ibTimeTable.setOnClickListener(this);
        ibEvents.setOnClickListener(this);
        ibMarks.setOnClickListener(this);
        ibSettings.setOnClickListener(this);
        ibColors.setOnClickListener(this);
        try {
            v.setBackgroundColor(CommonObjects.getDbHelper().getColors().get(0).getBackgroundColor());
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_APP_COLORS, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibSubjects:
                CommonMethods.callFragment(SubjectsFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),SubjectsFragment.TAG);
                break;
            case R.id.ibTimeTable:
                int size=0;
                try {
                    size=CommonObjects.getDbHelper().getSubjects().size();
                } catch (Exception e) {
                }
                if(size!=0) {
                    CommonMethods.callFragment(TimetableFragment.createFragment(), R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, getActivity(), TimetableFragment.TAG);
                }
                else
                {
                    CommonMethods.showToast(Messages.PLEASE_ADD_A_SUBJECT_FIRST, Toast.LENGTH_LONG);
                }
                break;
            case R.id.ibEvents:
                CommonMethods.callFragment(EventsFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),SubjectsFragment.TAG);
                break;
            case R.id.ibMarks:
                CommonMethods.callFragment(MarksFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),MarksFragment.TAG);
                break;
            case R.id.ibSettings:
                CommonMethods.callFragment(SettingsFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),SettingsFragment.TAG);
                break;
            case R.id.ibColors:
                ColorFragment colorFragment=ColorFragment.createFragment();
                colorFragment.setOnUpdateNotifier(new MainActivity.OnUpdateNotifier() {
                    @Override
                    public void onUpdate() {
                        try {
                            getView().setBackgroundColor(CommonObjects.getDbHelper().getColors().get(0).getBackgroundColor());
                        } catch (Exception e) {
                            CommonMethods.showToast(Messages.FAILED_TO_LOAD_APP_COLORS, Toast.LENGTH_LONG);
                        }                    }
                });
                CommonMethods.callFragment(colorFragment,R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),ColorFragment.TAG);
                break;
        }
    }
}
