package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.adapters.ListItemCalenderAdapter;

public class CalenderFragment extends Fragment {
    private TextView tvTitle;
    private ImageButton ibLeft;
    private ImageButton ibRight;
    private int dateCount=0;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.calender_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        ibLeft = (ImageButton) v.findViewById(R.id.btnLeft);
        ibRight = (ImageButton) v.findViewById(R.id.btnRight);
        ibLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount--;
                tvTitle.setText(CommonMethods.getMonthDateByCount(dateCount));
                setDatesByCount(dateCount);
            }
        });
        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount++;
                tvTitle.setText(CommonMethods.getMonthDateByCount(dateCount));
                setDatesByCount(dateCount);
            }
        });
        listView=(ListView)v.findViewById(R.id.listView);
        setDatesByCount(dateCount);
        tvTitle.setText(CommonMethods.getMonthDateByCount(dateCount));
    }

    private void setDatesByCount(int count) {
        listView.setAdapter(new ListItemCalenderAdapter(getContext(),CommonMethods.getCalenderDates(dateCount),R.layout.list_item_calender));
    }
}
