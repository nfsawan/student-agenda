package com.sofwen.studentagenda.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.business.objects.Timetable;
import com.sofwen.studentagenda.fragments.dialogs.DaysFragmentDialog;
import com.sofwen.studentagenda.fragments.dialogs.SelectSubjectFragmentDialog;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddTimetableFragment extends Fragment {
    public static final String TAG = AddTimetableFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private EditText etDay;
    private EditText etSubject;
    private EditText etRome;
    private EditText etStartTime;
    private EditText etEndTime;
    private int subjectId=-1;

    private Timetable timetable;

    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }

    public static AddTimetableFragment createFragment() {
        AddTimetableFragment addSubjectFragment = new AddTimetableFragment();
        return addSubjectFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.add_timetable_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        tvTitle.setText("Add Timetable");
        btnLeft = (Button) v.findViewById(R.id.btnLeft);
        btnRight = (Button) v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etDay.getText().toString().equals(""))
                {
                    CommonMethods.showToast(Messages.DAY_REQUIRED, Toast.LENGTH_LONG);
                }
                else if(etSubject.getText().toString().equals(""))
                {
                    CommonMethods.showToast(Messages.SUBJECT_REQUIRED, Toast.LENGTH_LONG);
                }
                else if(etStartTime.getText().toString().equals(""))
                {
                    CommonMethods.showToast(Messages.START_TIME_REQUIRED, Toast.LENGTH_LONG);
                }
                else {
                    Timetable timetable=new Timetable();
                    timetable.setDay(etDay.getText().toString());
                    timetable.setSubjectId(subjectId);
                    timetable.setStartTime(etStartTime.getText().toString());
                    timetable.setEndTime(etEndTime.getText().toString());
                    timetable.setRome(etRome.getText().toString());
                    try {
                        CommonObjects.getDbHelper().insertTimetable(timetable);
                        getFragmentManager().popBackStack();
                        CommonMethods.showToast(Messages.NEW_TIMETABLE_ADDED_SUCCESSFULLY, Toast.LENGTH_LONG);
                    } catch (Exception e) {
                        CommonMethods.showToast(Messages.FAILED_TO_ADD_TIMETABLE, Toast.LENGTH_LONG);
                    }
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        etDay = (EditText) v.findViewById(R.id.etDay);
        v.findViewById(R.id.flDay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DaysFragmentDialog daysFragmentDialog = new DaysFragmentDialog();
                daysFragmentDialog.setBtnSelectClickListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        daysFragmentDialog.dismiss();
                        etDay.setText(((Button) v).getText());
                    }
                });
                daysFragmentDialog.show(getFragmentManager(), "");
            }
        });
        v.findViewById(R.id.flSubject).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SelectSubjectFragmentDialog selectSubjectFragmentDialog = new SelectSubjectFragmentDialog();
                selectSubjectFragmentDialog.setListItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectSubjectFragmentDialog.dismiss();
                        Subject subject=((Subject) parent.getAdapter().getItem(position));
                        etSubject.setText(subject.getShortName());
                        subjectId=subject.getId();
                    }
                });
                selectSubjectFragmentDialog.show(getFragmentManager(), "");
            }
        });
        v.findViewById(R.id.flStartTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(), "", new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                        etStartTime.setText(hourOfDay + ":" + minute);
                    }
                }, getActivity().getSupportFragmentManager());
            }
        });
        v.findViewById(R.id.flEndTime).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(), "", new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {
                        etEndTime.setText(hourOfDay + ":" + minute);
                    }
                }, getActivity().getSupportFragmentManager());
            }
        });
        etSubject = (EditText) v.findViewById(R.id.etSubject);
        etRome = (EditText) v.findViewById(R.id.etRome);
        etStartTime = (EditText) v.findViewById(R.id.etStartTime);
        etEndTime = (EditText) v.findViewById(R.id.etEndTime);
        CommonMethods.setupUI(v, getContext());

        if(timetable!=null)
        {
            etDay.setText(timetable.getDay());
            etSubject.setText(timetable.getSubject().getShortName());
            etRome.setText(timetable.getRome());
            etStartTime.setText(timetable.getStartTime());
            etEndTime.setText(timetable.getEndTime());
            subjectId=timetable.getSubjectId();
        }
    }

}
