package com.sofwen.studentagenda.fragments.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.adapters.ListItemSelectSubjectAdapter;
import com.sofwen.studentagenda.adapters.ListItemSubjectAdapter;

public class SelectSubjectFragmentDialog extends DialogFragment {
    private View customView;
    private View.OnClickListener btnCancelClickListener =new View.OnClickListener() {
        @Override
        public void onClick(View v) {
dismiss();
        }
    };
    private AdapterView.OnItemClickListener listItemClickListener=new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            dismiss();
        }
    };

    public void setListItemClickListener(AdapterView.OnItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }

    public void setBtnCancelClickListener(View.OnClickListener btnCancelClickListener) {
        this.btnCancelClickListener = btnCancelClickListener;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        customView = getActivity().getLayoutInflater().inflate(
                R.layout.select_subject_fragment_dialog, null);

        Dialog dialog=new Dialog(getContext(),R.style.Base_Theme_AppCompat_Dialog);
        dialog.setCancelable(false);
        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        dismiss();
                        return true;
                    }
                }
                return false;
            }
        });
        Button btnCancel=(Button)customView.findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(btnCancelClickListener);

        ListView listView=(ListView)customView.findViewById(R.id.listView);
        listView.setOnItemClickListener(listItemClickListener);
        try {
            listView.setAdapter(new ListItemSelectSubjectAdapter(getContext(), CommonObjects.getDbHelper().getSubjects(),R.layout.list_item_name));
            CommonMethods.setListViewHeightBasedOnItems(listView,0);
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_SUBJECT, Toast.LENGTH_LONG);
        }
        dialog.setCanceledOnTouchOutside(false);
        dialog.setContentView(customView);

        return dialog;
    }


}
