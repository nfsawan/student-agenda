package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.fragments.dialogs.DaysFragmentDialog;


public class SettingsFragment extends Fragment {
    public static final String TAG = SettingsFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private TextView tvFirstDayOfWeek;

    public static SettingsFragment createFragment() {
        SettingsFragment addSubjectFragment = new SettingsFragment();
        return addSubjectFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.settings_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("Settings");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setVisibility(View.INVISIBLE);
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        v.findViewById(R.id.llFirstDayOfWeek).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final DaysFragmentDialog daysFragmentDialog = new DaysFragmentDialog();
                daysFragmentDialog.setTitle("Select first day of week");
                daysFragmentDialog.setBtnSelectClickListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        daysFragmentDialog.dismiss();
                        CommonMethods.setStringPreference(getContext(),getString(R.string.app_name),getString(R.string.first_day_of_the_week),((Button) v).getText().toString());
                        tvFirstDayOfWeek.setText(CommonMethods.getStringPreference(getContext(),getString(R.string.app_name),getString(R.string.first_day_of_the_week),getString(R.string.monday)));
                    }
                });
                daysFragmentDialog.show(getFragmentManager(), "");
            }
        });
        v.findViewById(R.id.llData).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(DataFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),DataFragment.TAG);
            }
        });
        v.findViewById(R.id.llContactUs).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.composeEmail(getContext(),"","","","");
            }
        });
        v.findViewById(R.id.llLikeThisApp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.rate(getContext());
            }
        });
        tvFirstDayOfWeek=(TextView)v.findViewById(R.id.tvFirstDayOfWeek);
        tvFirstDayOfWeek.setText(CommonMethods.getStringPreference(getContext(),getString(R.string.app_name),getString(R.string.first_day_of_the_week),getString(R.string.monday)));
    }

}
