package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.adapters.ListItemTimetableAdapter;

import java.util.ArrayList;

public class WeekFragment extends Fragment {
    private TextView tvTitle;
    private ImageButton ibLeft;
    private ImageButton ibRight;
    private int dateCount=0;
    private TextView tvMon;
    private TextView tvTue;
    private TextView tvWed;
    private TextView tvThu;
    private TextView tvFri;
    private TextView tvSat;
    private TextView tvSun;
    private ListView listView;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.week_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        ibLeft = (ImageButton) v.findViewById(R.id.btnLeft);
        ibRight = (ImageButton) v.findViewById(R.id.btnRight);
        ibLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount--;
                tvTitle.setText(CommonMethods.getWeekDateByCount(dateCount));
                setDatesByCount(dateCount);
            }
        });
        ibRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dateCount++;
                tvTitle.setText(CommonMethods.getWeekDateByCount(dateCount));
                setDatesByCount(dateCount);
            }
        });
        tvTitle.setText(CommonMethods.getWeekDateByCount(dateCount));
        tvMon = (TextView) v.findViewById(R.id.tvMon);
        tvTue = (TextView) v.findViewById(R.id.tvTue);
        tvWed = (TextView) v.findViewById(R.id.tvWed);
        tvThu = (TextView) v.findViewById(R.id.tvThu);
        tvFri = (TextView) v.findViewById(R.id.tvFri);
        tvSat = (TextView) v.findViewById(R.id.tvSat);
        tvSun = (TextView) v.findViewById(R.id.tvSun);
        setDatesByCount(dateCount);
        listView=(ListView)v.findViewById(R.id.listView);
        try {
            listView.setAdapter(new ListItemTimetableAdapter(getContext(), CommonObjects.getDbHelper().getTimetables(),R.layout.list_item_timetable_monday));
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_TIMETABLE, Toast.LENGTH_LONG);
        }
    }

    @Override
    public void setMenuVisibility(boolean menuVisible) {
        super.setMenuVisibility(menuVisible);
        if (menuVisible) {
            try {
                if (listView != null) {
                    listView.setAdapter(new ListItemTimetableAdapter(getContext(), CommonObjects.getDbHelper().getTimetables(), R.layout.list_item_timetable_monday));
                }
            } catch (Exception e) {
                CommonMethods.showToast(Messages.FAILED_TO_LOAD_TIMETABLE, Toast.LENGTH_LONG);
            }
        }
    }

    private void setDatesByCount(int count)
    {
        ArrayList<String> dates=CommonMethods.getWeekDates(count);
        tvSun.setText(dates.get(0));
        tvMon.setText(dates.get(1));
        tvTue.setText(dates.get(2));
        tvWed.setText(dates.get(3));
        tvThu.setText(dates.get(4));
        tvFri.setText(dates.get(5));
        tvSat.setText(dates.get(6));
    }

}
