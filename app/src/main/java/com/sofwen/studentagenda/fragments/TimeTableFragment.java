package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.adapters.ListItemTimetableAdapter;

public class TimetableFragment extends Fragment {
    public static final String TAG = TimetableFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private ListView listView;
    private LinearLayout llMonday;
    private LinearLayout llSunday;
    private int layout;

    public static TimetableFragment createFragment() {
        TimetableFragment timeTableFragment = new TimetableFragment();
        return timeTableFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.timetable_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.timetable));
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Add");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(AddTimetableFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),AddTimetableFragment.TAG);
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        llMonday=(LinearLayout)v.findViewById(R.id.llMonday);
        llSunday=(LinearLayout)v.findViewById(R.id.llSunday);
        if(CommonMethods.getStringPreference(getContext(),getString(R.string.app_name),getString(R.string.first_day_of_the_week),getString(R.string.monday)).equals(getString(R.string.monday)))
        {
            llMonday.setVisibility(View.VISIBLE);
            llSunday.setVisibility(View.INVISIBLE);
            layout=R.layout.list_item_timetable_monday;
        }
        else
        {
            llMonday.setVisibility(View.INVISIBLE);
            llSunday.setVisibility(View.VISIBLE);
            layout=R.layout.list_item_timetable_sunday;
        }
        listView=(ListView)v.findViewById(R.id.listView);
        try {
            listView.setAdapter(new ListItemTimetableAdapter(getContext(),CommonObjects.getDbHelper().getTimetables(),layout));
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_TIMETABLE, Toast.LENGTH_LONG);
        }
        CommonMethods.setupUI(v,getContext());
    }

}
