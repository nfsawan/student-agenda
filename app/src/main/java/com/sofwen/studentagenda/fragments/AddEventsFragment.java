package com.sofwen.studentagenda.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Event;
import com.sofwen.studentagenda.business.objects.Subject;

import org.w3c.dom.Text;

import java.util.Calendar;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddEventsFragment extends Fragment {
    public static final String TAG = AddEventsFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private EditText etName;
    private TextView tvEventText;
    private CheckBox cbAddReminder;
    String eventText;
    private LinearLayout llReminderDate;
    private LinearLayout llReminderTime;
    private TextView tvDate;
    private TextView tvTime;
    private EditText etDescription;
    private TextView etReminderDate;
    private TextView etReminderTime;
    private int day,month,year,hour,minut;
//    CommonMethods.callDatePicker(this, "", datePickerListener, getSupportFragmentManager());

//    private EditText etShortName;
//    private EditText etTeacherName;
//    private EditText etTeacherEmail;
//    private EditText etSemester;
//    private EditText etYear;
//    private ImageView ivColor;

    public static AddEventsFragment createFragment() {
        AddEventsFragment addEventsFragment = new AddEventsFragment();
        return addEventsFragment;
    }

    public void setEventText(String eventText) {
        this.eventText = eventText;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.add_events_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        getCurrentDateTime();
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("Add Events");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        cbAddReminder= (CheckBox) v.findViewById(R.id.cbAddReminder);
        llReminderDate= (LinearLayout) v.findViewById(R.id.llReminderDate);
        llReminderTime= (LinearLayout) v.findViewById(R.id.llReminderTime);
        etDescription= (EditText) v.findViewById(R.id.etDescription);
        tvDate= (TextView) v.findViewById(R.id.tvDate);
        etReminderDate= (TextView) v.findViewById(R.id.etReminderDate);
        etReminderTime= (TextView) v.findViewById(R.id.etReminderTime);
        etReminderDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callDatePicker(getActivity(), "", reminderDatePickerListener, getActivity().getSupportFragmentManager());
            }
        });
        etReminderTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(),"",onReminderTimeSetListener,getActivity().getSupportFragmentManager());

            }
        });
//        tvDate.setText(Calend);
        tvTime= (TextView) v.findViewById(R.id.tvTime);
        tvTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callTimePicker(getActivity(),"",onTimeSetListener,getActivity().getSupportFragmentManager());
            }
        });
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callDatePicker(getActivity(), "", datePickerListener, getActivity().getSupportFragmentManager());
            }
        });

        cbAddReminder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    llReminderDate.setVisibility(View.VISIBLE);
                    llReminderTime.setVisibility(View.VISIBLE);
                    etReminderDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
                    etReminderTime.setText(hour + ":"+ minut +"");
                }else {
                    llReminderDate.setVisibility(View.GONE);
                    llReminderTime.setVisibility(View.GONE);
                }
            }
        });
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etName.getText().toString().equals(""))
                {
                    CommonMethods.showToast(Messages.EVENT_NAME_REQUIRED, Toast.LENGTH_LONG);
                }
                else {
                    Event event=new Event();
                    event.setName(etName.getText().toString());
                    event.setType(eventText);
//                    subject.setColor(((ColorDrawable) ivColor.getBackground()).getColor());
                    event.setDate(tvDate.getText().toString());
                    event.setTime(tvTime.getText().toString());
                    Log.d("**TImeSendingToDB",tvTime.getText().toString());
                    event.setDescription(etDescription.getText().toString());
                    event.setSubjectId(0);
                    if (cbAddReminder.isChecked()){
                        event.setReminderId("1000");
                        event.setReminderDate(etReminderDate.getText().toString());
                    event.setReminderTime(etReminderTime.getText().toString());
                    }
//                    subject.setYear(etYear.getText().toString());
                    try {
                        CommonObjects.getDbHelper().insertEvent(event);
                        getFragmentManager().popBackStack();
                        CommonMethods.showToast(Messages.EVENT_ADDED_SUCCESSFULLY, Toast.LENGTH_LONG);
                    } catch (Exception e) {
                        CommonMethods.showToast(Messages.FAILED_TO_ADD_EVENT, Toast.LENGTH_LONG);
                    }
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        etName=(EditText)v.findViewById(R.id.etName);
        tvEventText = (TextView) v.findViewById(R.id.tvEventText);
        tvEventText.setText(eventText);

//        etShortName=(EditText)v.findViewById(R.id.etShortName);
//        etTeacherName=(EditText)v.findViewById(R.id.etTeacherName);
//        etTeacherEmail=(EditText)v.findViewById(R.id.etTeacherEmail);
//        etSemester=(EditText)v.findViewById(R.id.etSemester);
//        etYear=(EditText)v.findViewById(R.id.etYear);
//        ivColor=(ImageView)v.findViewById(R.id.ivColor);
//        ((LinearLayout)v.findViewById(R.id.llColor)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final ColorPicker colorPicker = new ColorPicker(getActivity());
//                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
//                    @Override
//                    public void setOnFastChooseColorListener(int position, int color) {
//                        ivColor.setBackgroundColor(color);
//                        colorPicker.dismissDialog();
//                    }
//                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                }).show();
//            }
//        });

        tvDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
        CommonMethods.setupUI(v,getContext());
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year,
                              int month, int day) {
         tvDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
        }
    };
    private DatePickerDialog.OnDateSetListener reminderDatePickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePickerDialog datePickerDialog, int year,
                              int month, int day) {
            etReminderDate.setText(day + ", " + CommonMethods.getMonthFromInt((month + 1)) + " " + year + "");
        }
    };
    private TimePickerDialog.OnTimeSetListener onTimeSetListener= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i1) {

            tvTime.setText(i + ":"+ i1 +"");
        }
    };
    private TimePickerDialog.OnTimeSetListener onReminderTimeSetListener= new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(RadialPickerLayout radialPickerLayout, int i, int i1) {

            etReminderTime.setText(i + ":"+ i1 +"");
        }
    };
    private void getCurrentDateTime(){
        Calendar calendar = Calendar.getInstance();
        day = calendar.get(Calendar.DAY_OF_MONTH);
        month = calendar.get(Calendar.MONTH);
        year = calendar.get(Calendar.YEAR);
        hour = calendar.get(Calendar.HOUR_OF_DAY);
        minut = calendar.get(Calendar.MINUTE);
    }
}
