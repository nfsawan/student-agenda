package com.sofwen.studentagenda.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.adapters.ListItemSubjectAdapter;
import com.sofwen.studentagenda.business.objects.Subject;

import java.util.ArrayList;

public class SubjectsFragment extends Fragment {
    public static final String TAG = SubjectsFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private ListView listView;

    public static SubjectsFragment createFragment() {
        SubjectsFragment subjectsFragment = new SubjectsFragment();
        return subjectsFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.subjects_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText(getString(R.string.subjects));
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Add");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethods.callFragment(AddSubjectFragment.createFragment(),R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,getActivity(),AddSubjectFragment.TAG);
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        listView=(ListView)v.findViewById(R.id.listView);
        try {
            listView.setAdapter(new ListItemSubjectAdapter(getContext(),CommonObjects.getDbHelper().getSubjects(),R.layout.list_item_subject));
        } catch (Exception e) {
            CommonMethods.showToast(Messages.FAILED_TO_LOAD_SUBJECT, Toast.LENGTH_LONG);
        }
        CommonMethods.setupUI(v,getContext());
    }

}
