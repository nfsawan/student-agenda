package com.sofwen.studentagenda.fragments;

import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Subject;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddSubjectFragment extends Fragment {
    public static final String TAG = AddSubjectFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private EditText etName;
    private EditText etShortName;
    private EditText etTeacherName;
    private EditText etTeacherEmail;
    private EditText etSemester;
    private EditText etYear;
    private ImageView ivColor;

    public static AddSubjectFragment createFragment() {
        AddSubjectFragment addSubjectFragment = new AddSubjectFragment();
        return addSubjectFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.add_subject_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("Add Subject");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etShortName.getText().toString().equals(""))
                {
                    CommonMethods.showToast(Messages.SUBJECT_SHORT_NAME_REQUIRED, Toast.LENGTH_LONG);
                }
                else {
                    Subject subject=new Subject();
                    subject.setName(etName.getText().toString());
                    subject.setShortName(etShortName.getText().toString());
                    subject.setColor(((ColorDrawable) ivColor.getBackground()).getColor());
                    subject.setTeacherName(etTeacherName.getText().toString());
                    subject.setTeacherEmail(etTeacherEmail.getText().toString());
                    subject.setSemester(etSemester.getText().toString());
                    subject.setYear(etYear.getText().toString());
                    try {
                        CommonObjects.getDbHelper().insertSubject(subject);
                        getFragmentManager().popBackStack();
                        CommonMethods.showToast(Messages.NEW_SUBJECT_ADDED_SUCCESSFULLY, Toast.LENGTH_LONG);
                    } catch (Exception e) {
                        CommonMethods.showToast(Messages.FAILED_TO_ADD_SUBJECT, Toast.LENGTH_LONG);
                    }
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        etName=(EditText)v.findViewById(R.id.etName);
        etShortName=(EditText)v.findViewById(R.id.etShortName);
        etTeacherName=(EditText)v.findViewById(R.id.etTeacherName);
        etTeacherEmail=(EditText)v.findViewById(R.id.etTeacherEmail);
        etSemester=(EditText)v.findViewById(R.id.etSemester);
        etYear=(EditText)v.findViewById(R.id.etYear);
        ivColor=(ImageView)v.findViewById(R.id.ivColor);
        ivColor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ColorPicker colorPicker = new ColorPicker(getActivity());
                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
                    @Override
                    public void setOnFastChooseColorListener(int position, int color) {
                        ivColor.setBackgroundColor(color);
                        colorPicker.dismissDialog();
                    }
                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
            }
        });
        CommonMethods.setupUI(v,getContext());
    }

}
