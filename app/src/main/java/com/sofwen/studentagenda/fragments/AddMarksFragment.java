package com.sofwen.studentagenda.fragments;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Mark;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.fragments.dialogs.SelectSubjectFragmentDialog;

import petrov.kristiyan.colorpicker.ColorPicker;

public class AddMarksFragment extends Fragment {
    public static final String TAG = AddMarksFragment.class.getSimpleName();
    private TextView tvTitle;
    private Button btnLeft;
    private Button btnRight;
    private EditText etName;
    private TextView tvSubject;
    private EditText etMark;
    private EditText etTitle;
    private EditText etMarkDescription;
    private EditText etWeight;
    private Mark mark;
    private int subjectId=-1;

    public void setMarks(Mark mark) {
        this.mark = mark;
    }
    public static AddMarksFragment createFragment() {
        return new AddMarksFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(
                R.layout.add_marks_fragment, container, false);
        init(v);
        return v;
    }

    private void init(View v) {
        tvTitle=(TextView)v.findViewById(R.id.tvTitle);
        tvTitle.setText("New Mark");
        btnLeft=(Button)v.findViewById(R.id.btnLeft);
        btnRight=(Button)v.findViewById(R.id.btnRight);
        btnLeft.setText("Back");
        btnLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
        btnRight.setText("Save");
        btnRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tvSubject.getText().toString().equals("")) {
                    CommonMethods.showToast(Messages.SUBJECT_REQUIRED, Toast.LENGTH_LONG);
                } else if (etMark.getText().toString().equals("")) {
                    CommonMethods.showToast(Messages.MARKS_REQUIRED, Toast.LENGTH_LONG);
                } else {
                    Mark mark = new Mark();
                    mark.setMark(Integer.parseInt(etMark.getText().toString()));

                    mark.setWeight(Float.parseFloat(etWeight.getText().toString()));
                    mark.setDescription(etMarkDescription.getText().toString());
                    mark.setTitle(etTitle.getText().toString());
                    mark.setSubjectId(subjectId);
//                    subject.setSemester(etSemester.getText().toString());
//                    subject.setYear(etYear.getText().toString());
                    try {
                        CommonObjects.getDbHelper().insertMark(mark);
                        getFragmentManager().popBackStack();
                        CommonMethods.showToast(Messages.NEW_MARKS_ADDED_SUCCESSFULLY, Toast.LENGTH_LONG);
                    } catch (Exception e) {
                        CommonMethods.showToast(Messages.FAILED_TO_ADD_MARKS, Toast.LENGTH_LONG);
                    }
                }
            }
        });
        v.setFocusableInTouchMode(true);
        v.requestFocus();
        v.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_BACK) {
                        getFragmentManager().popBackStack();
                        return true;
                    }
                }
                return false;
            }
        });
        tvSubject=(TextView)v.findViewById(R.id.tvSubject);
        tvSubject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final SelectSubjectFragmentDialog selectSubjectFragmentDialog = new SelectSubjectFragmentDialog();
                selectSubjectFragmentDialog.setListItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        selectSubjectFragmentDialog.dismiss();
                        Subject subject=((Subject) parent.getAdapter().getItem(position));
                        tvSubject.setText(subject.getShortName());
                        subjectId=subject.getId();
                    }
                });
                selectSubjectFragmentDialog.show(getFragmentManager(), "");
            }
        });
        etMark=(EditText)v.findViewById(R.id.etMark);
        etMarkDescription=(EditText)v.findViewById(R.id.etMarkDescription);
        etTitle=(EditText)v.findViewById(R.id.etTitle);
        etWeight=(EditText)v.findViewById(R.id.etWeight);
//        etYear=(EditText)v.findViewById(R.id.etYear);
//        ivColor=(ImageView)v.findViewById(R.id.ivColor);
//        ((LinearLayout)v.findViewById(R.id.llColor)).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                final ColorPicker colorPicker = new ColorPicker(getActivity());
//                colorPicker.setFastChooser(new ColorPicker.OnFastChooseColorListener() {
//                    @Override
//                    public void setOnFastChooseColorListener(int position, int color) {
//                        ivColor.setBackgroundColor(color);
//                        colorPicker.dismissDialog();
//                    }
//                }).setPositiveButton("Cancel", new ColorPicker.OnButtonListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                    }
//                }).show();
//            }
//        });
        CommonMethods.setupUI(v,getContext());
    }

}
