package com.sofwen.studentagenda.database;

import java.lang.reflect.Type;
import java.sql.Time;
import java.util.ArrayList;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.sofwen.studentagenda.business.objects.Color;
import com.sofwen.studentagenda.business.objects.Event;
import com.sofwen.studentagenda.business.objects.Mark;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.business.objects.Timetable;

public class DBHelper extends SQLiteOpenHelper {
	private static final String DATABSE_NAME = "StudentAgenda.db";
	private static final int DATABSE_VERSION = 1;
	SQLiteDatabase dBase = null;

	private static class SubjectTable {
		private static final String TABLE_NAME = "Subject";
		private static final String COL_ID = "id";
		private static final String COL_NAME = "name";
		private static final String COL_SHORT_NAME = "short_name";
		private static final String COL_COLOR = "color";
		private static final String COL_TEACHER_NAME = "teacher_name";
		private static final String COL_TEACHER_EMAIL = "teacher_email";
		private static final String COL_SEMESTER = "semester";
		private static final String COL_YEAR = "year";

		private static ContentValues getSubjectValues(
				Subject subject) {
			ContentValues values = new ContentValues();
			values.put(COL_NAME, subject.getName());
			values.put(COL_SHORT_NAME, subject.getShortName());
			values.put(COL_COLOR, subject.getColor());
			values.put(COL_TEACHER_NAME, subject.getTeacherName());
			values.put(COL_TEACHER_EMAIL, subject.getTeacherEmail());
			values.put(COL_SEMESTER, subject.getSemester());
			values.put(COL_YEAR, subject.getYear());
			return values;
		}
	}

	private static class EventTable {
		private static final String TABLE_NAME = "Event";
		private static final String COL_ID = "id";
		private static final String COL_NAME = "name";
		private static final String COL_TYPE = "type";
		private static final String COL_DATE = "date";
		private static final String COL_TIME = "time";
		private static final String COL_REMINDER_DATE = "reminder_date";
		private static final String COL_REMINDER_TIME = "reminder_time";
		private static final String COL_DESCRIPTION = "description";
		private static final String COL_SUBJECT_ID = "subject_id";
		private static final String COL_REMINDER_ID = "reminder_id";

		private static ContentValues getEventValues(
				Event event) {
			ContentValues values = new ContentValues();
			values.put(COL_NAME, event.getName());
			values.put(COL_TYPE, event.getType());
			values.put(COL_DATE, event.getDate());
			values.put(COL_TIME, event.getTime());
			values.put(COL_REMINDER_DATE,event.getReminderDate());
			values.put(COL_REMINDER_TIME,event.getReminderTime());
			values.put(COL_DESCRIPTION, event.getDescription());
			values.put(COL_SUBJECT_ID, event.getSubjectId());
			values.put(COL_REMINDER_ID, event.getReminderId());
			return values;
		}

	}

	private static class TimetableTable {
		private static final String TABLE_NAME = "Timetable";
		private static final String COL_ID = "id";
		private static final String COL_DAY = "day";
		private static final String COL_SUBJECT_ID = "subject_id";
		private static final String COL_START_TIME = "start_time";
		private static final String COL_END_TIME = "end_time";
		private static final String COL_ROME = "rome";

		private static ContentValues getTimetableValues(
				Timetable timetable) {
			ContentValues values = new ContentValues();
			values.put(COL_DAY, timetable.getDay());
			values.put(COL_SUBJECT_ID, timetable.getSubjectId());
			values.put(COL_START_TIME, timetable.getStartTime());
			values.put(COL_END_TIME, timetable.getEndTime());
			values.put(COL_ROME, timetable.getRome());
			return values;
		}

	}

	private static class MarkTable {
		private static final String TABLE_NAME = "Mark";
		private static final String COL_ID = "id";
		private static final String COL_SUBJECT_ID = "subject_id";
		private static final String COL_MARK = "mark";
		private static final String COL_WEIGHT = "weight";
		private static final String COL_TITLE = "title";
		private static final String COL_DESCRIPTION = "description";

		private static ContentValues getMarkValues(
				Mark mark) {
			ContentValues values = new ContentValues();
			values.put(COL_ID, mark.getId());
			values.put(COL_SUBJECT_ID, mark.getSubjectId());
			values.put(COL_MARK, mark.getMark());
			values.put(COL_WEIGHT, mark.getWeight());
			values.put(COL_TITLE, mark.getTitle());
			values.put(COL_DESCRIPTION, mark.getDescription());
			return values;
		}

	}

	private static class ColorTable {
		private static final String TABLE_NAME = "Color";
		private static final String COL_ID = "id";
		private static final String COL_BACKGROUND_COLOR = "background_color";
		private static final String COL_EXAM_COLOR = "exam_color";
		private static final String COL_ACTIVITY_COLOR = "activity_color";
		private static final String COL_COURSEWORK_HOMEWORK_COLOR = "coursework_homework_color";
		private static final String COL_RETURN_BOOK_COLOR = "return_book_color";

		private static ContentValues getColorValues(
				Color color) {
			ContentValues values = new ContentValues();
			values.put(COL_BACKGROUND_COLOR, color.getBackgroundColor());
			values.put(COL_EXAM_COLOR, color.getExamColor());
			values.put(COL_ACTIVITY_COLOR, color.getActivityColor());
			values.put(COL_COURSEWORK_HOMEWORK_COLOR, color.getCourseworkHomeworkColor());
			values.put(COL_RETURN_BOOK_COLOR, color.getReturnBookColor());
			return values;
		}
	}

	public DBHelper(Context context) {
		super(context, DATABSE_NAME, null, DATABSE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL("CREATE TABLE " + SubjectTable.TABLE_NAME + " ("
					+ SubjectTable.COL_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ SubjectTable.COL_NAME + " TEXT ,"
					+ SubjectTable.COL_SHORT_NAME + " TEXT ,"
					+ SubjectTable.COL_COLOR + " INTEGER ,"
					+ SubjectTable.COL_TEACHER_NAME + " TEXT ,"
					+ SubjectTable.COL_TEACHER_EMAIL + " TEXT ,"
					+ SubjectTable.COL_SEMESTER + " TEXT ,"
					+ SubjectTable.COL_YEAR + " TEXT )");
			db.execSQL("CREATE TABLE " + EventTable.TABLE_NAME + " ("
					+ EventTable.COL_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ EventTable.COL_NAME + " TEXT ,"
					+ EventTable.COL_TYPE + " TEXT ,"
					+ EventTable.COL_DATE + " TEXT ,"
					+ EventTable.COL_TIME + " TEXT ,"
					+ EventTable.COL_REMINDER_DATE + " TEXT ,"
					+ EventTable.COL_REMINDER_TIME + " TEXT ,"
					+ EventTable.COL_DESCRIPTION + " TEXT ,"
					+ EventTable.COL_SUBJECT_ID + " INTEGER ,"
					+ EventTable.COL_REMINDER_ID + " TEXT )");
			db.execSQL("CREATE TABLE " + TimetableTable.TABLE_NAME + " ("
					+ TimetableTable.COL_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ TimetableTable.COL_DAY + " TEXT ,"
					+ TimetableTable.COL_SUBJECT_ID + " INTEGER ,"
					+ TimetableTable.COL_START_TIME + " TEXT ,"
					+ TimetableTable.COL_END_TIME + " TEXT ,"
					+ TimetableTable.COL_ROME + " TEXT )");
			db.execSQL("CREATE TABLE " + MarkTable.TABLE_NAME + " ("
					+ MarkTable.COL_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ MarkTable.COL_SUBJECT_ID + " INTEGER ,"
					+ MarkTable.COL_WEIGHT + " REAL ,"
					+ MarkTable.COL_MARK + " INTEGER ,"
					+ MarkTable.COL_TITLE + " TEXT ,"
					+ MarkTable.COL_DESCRIPTION + " TEXT )");
			db.execSQL("CREATE TABLE " + ColorTable.TABLE_NAME + " ("
					+ ColorTable.COL_ID
					+ " INTEGER PRIMARY KEY AUTOINCREMENT ,"
					+ ColorTable.COL_BACKGROUND_COLOR + " INTEGER ,"
					+ ColorTable.COL_EXAM_COLOR+ " INTEGER ,"
					+ ColorTable.COL_ACTIVITY_COLOR + " INTEGER ,"
					+ ColorTable.COL_COURSEWORK_HOMEWORK_COLOR+ " INTEGER ,"
					+ ColorTable.COL_RETURN_BOOK_COLOR + " INTEGER )");
		} catch (Exception ex) {
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

	public void openDB(boolean read) {
		if (read)
			dBase = this.getReadableDatabase();
		else
			dBase = this.getWritableDatabase();
	}

	public void closeDB() {
		if(dBase!=null)
		dBase.close();
		dBase = null;
	}

	public void insertSubject(Subject subject) throws Exception {
		try {
			openDB(false);

			dBase.insert(SubjectTable.TABLE_NAME,
					SubjectTable.COL_ID,
					SubjectTable.getSubjectValues(subject));
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void insertTimetable(Timetable timetable) throws Exception {
		try {
			openDB(false);

			dBase.insert(TimetableTable.TABLE_NAME,
					TimetableTable.COL_ID,
					TimetableTable.getTimetableValues(timetable));
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void insertEvent(Event event) throws Exception {
		try {
			openDB(false);

			dBase.insert(EventTable.TABLE_NAME,
					EventTable.COL_ID,
					EventTable.getEventValues(event));
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void insertMark(Mark mark) throws Exception {
		try {
			openDB(false);

			dBase.insert(MarkTable.TABLE_NAME,
					MarkTable.COL_ID,
					MarkTable.getMarkValues(mark));
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void insertColor(Color color) throws Exception {
		try {
			openDB(false);

			dBase.insert(ColorTable.TABLE_NAME,
					ColorTable.COL_ID,
					ColorTable.getColorValues(color));
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}


	public ArrayList<Subject> getSubjects() throws Exception {
		Cursor cur;
		ArrayList<Subject> subjects = new ArrayList<>();
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ SubjectTable.TABLE_NAME, null);
			if (cur.moveToFirst()) {
				do {
					Subject subject=new Subject();
					subject.setId(cur.getInt(0));
					subject.setName(cur.getString(1));
					subject.setShortName(cur.getString(2));
					subject.setColor(cur.getInt(3));
					subject.setTeacherName(cur.getString(4));
					subject.setTeacherEmail(cur.getString(5));
					subject.setSemester(cur.getString(6));
					subject.setYear(cur.getString(7));
					subjects.add(subject);
				} while (cur.moveToNext());
			}
			cur.close();
			return subjects;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public Subject getSubjectById(int id) throws Exception {
		Cursor cur;
		Subject subject =null;
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ SubjectTable.TABLE_NAME+" where "+SubjectTable.COL_ID+"='"+id+"'", null);
			if (cur.moveToFirst()) {
				do {
					subject=new Subject();
					subject.setId(cur.getInt(0));
					subject.setName(cur.getString(1));
					subject.setShortName(cur.getString(2));
					subject.setColor(cur.getInt(3));
					subject.setTeacherName(cur.getString(4));
					subject.setTeacherEmail(cur.getString(5));
					subject.setSemester(cur.getString(6));
					subject.setYear(cur.getString(7));
				} while (cur.moveToNext());
			}
			cur.close();
			return subject;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}


	public ArrayList<Event> getEvents() throws Exception {
		Cursor cur;
		ArrayList<Event> events = new ArrayList<>();
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ EventTable.TABLE_NAME, null);
			if (cur.moveToFirst()) {
				do {
					Event event=new Event();
					event.setId(cur.getInt(0));
					event.setName(cur.getString(1));
					event.setType(cur.getString(2));
					event.setDate(cur.getString(3));
					event.setTime(cur.getString(4));
					event.setReminderDate(cur.getString(5));
					event.setReminderTime(cur.getString(6));
					event.setDescription(cur.getString(7));
					event.setSubjectId(cur.getInt(8));
					event.setReminderId(cur.getString(9));
					events.add(event);
				} while (cur.moveToNext());
			}
			cur.close();
			return events;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public ArrayList<Timetable> getTimetables() throws Exception {
		Cursor cur;
		ArrayList<Timetable> timetables = new ArrayList<>();
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ TimetableTable.TABLE_NAME, null);
			if (cur.moveToFirst()) {
				do {
					Timetable timetable=new Timetable();
					timetable.setId(cur.getInt(0));
					timetable.setDay(cur.getString(1));
					timetable.setSubjectId(cur.getInt(2));
					timetable.setStartTime(cur.getString(3));
					timetable.setEndTime(cur.getString(4));
					timetable.setRome(cur.getString(5));
					timetable.setSubject(getSubjectById(timetable.getSubjectId()));
					timetables.add(timetable);
				} while (cur.moveToNext());
			}
			cur.close();
			return timetables;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public ArrayList<Mark> getMarks() throws Exception {
		Cursor cur;
		ArrayList<Mark> marks = new ArrayList<>();
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ MarkTable.TABLE_NAME, null);
			if (cur.moveToFirst()) {
				do {
					Mark mark=new Mark();
					mark.setId(cur.getInt(0));
					mark.setSubjectId(cur.getInt(1));
					mark.setWeight(cur.getFloat(2));
					mark.setMark(cur.getInt(3));
					mark.setTitle(cur.getString(4));
					mark.setDescription(cur.getString(5));
					marks.add(mark);
				} while (cur.moveToNext());
			}
			cur.close();
			return marks;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public ArrayList<Color> getColors() throws Exception {
		Cursor cur;
		ArrayList<Color> colors = new ArrayList<>();
		try {
			openDB(true);
			cur = dBase.rawQuery("select * from "
					+ ColorTable.TABLE_NAME, null);
			if (cur.moveToFirst()) {
				do {
					Color color=new Color();
					color.setId(cur.getInt(0));
					color.setBackgroundColor(cur.getInt(1));
					color.setExamColor(cur.getInt(2));
					color.setActivityColor(cur.getInt(3));
					color.setCourseworkHomeworkColor(cur.getInt(4));
					color.setReturnBookColor(cur.getInt(5));
					colors.add(color);
				} while (cur.moveToNext());
			}
			cur.close();
			return colors;
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void updateEvent(Event event) throws Exception {
		try {
			openDB(true);
			dBase.update(EventTable.TABLE_NAME,
					EventTable.getEventValues(event),
					EventTable.COL_ID + " = " + event.getId(), null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}

	}

	public void updateSubject(Subject subject) throws Exception {
		try {
			openDB(true);
			dBase.update(SubjectTable.TABLE_NAME,
					SubjectTable.getSubjectValues(subject),
					SubjectTable.COL_ID + " = " + subject.getId(), null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}

	}

	public void updateTimetable(Timetable timetable) throws Exception {
		try {
			openDB(true);
			dBase.update(TimetableTable.TABLE_NAME,
					TimetableTable.getTimetableValues(timetable),
					TimetableTable.COL_ID + " = " + timetable.getId(), null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}

	}

	public void updateMark(Mark mark) throws Exception {
		try {
			openDB(true);
			dBase.update(MarkTable.TABLE_NAME,
					MarkTable.getMarkValues(mark),
					MarkTable.COL_ID + " = " + mark.getId(), null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}

	}

	public void updateColor(Color color) throws Exception {
		try {
			openDB(true);
			dBase.update(ColorTable.TABLE_NAME,
					ColorTable.getColorValues(color),
					ColorTable.COL_ID + " = " + color.getId(), null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}

	}


	public void deleteSubjectById(int id) throws Exception {
		try {
			openDB(false);
			dBase.rawQuery("delete from "
					+ TimetableTable.TABLE_NAME+" where "+TimetableTable.COL_SUBJECT_ID+"='"+id+"'", null);
		} catch (Exception ex) {
			throw ex;
		} finally {
			closeDB();
		}
	}

	public void deleteEventById(int id) throws Exception {
		try {
			openDB(false);
			dBase.delete(EventTable.TABLE_NAME, EventTable.COL_ID + "="
					+ id, null);
		} catch (Exception ex) {

			throw ex;
		} finally {
			closeDB();
		}
	}

	public void deleteTimetableById(int id) throws Exception {
		try {
			openDB(false);
			dBase.delete(TimetableTable.TABLE_NAME, TimetableTable.COL_ID + "="
					+ id, null);
		} catch (Exception ex) {

			throw ex;
		} finally {
			closeDB();
		}
	}

	public void deleteTimetableBySubjectId(int id) throws Exception {
		try {
			openDB(false);
			dBase.delete(TimetableTable.TABLE_NAME, TimetableTable.COL_SUBJECT_ID + "="
					+ id, null);
		} catch (Exception ex) {

			throw ex;
		} finally {
			closeDB();
		}
	}

	public void deleteMarkById(int id) throws Exception {
		try {
			openDB(false);
			dBase.delete(MarkTable.TABLE_NAME, MarkTable.COL_ID + "="
					+ id, null);
		} catch (Exception ex) {

			throw ex;
		} finally {
			closeDB();
		}
	}


}
