package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.business.objects.Timetable;
import com.sofwen.studentagenda.fragments.UpdateSubjectFragment;
import com.sofwen.studentagenda.fragments.dialogs.MessageFragmentDialog;
import com.sofwen.studentagenda.fragments.dialogs.SelectEditorFragmentDialog;

import java.util.ArrayList;
import java.util.HashMap;


public class ListItemTimetableAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Timetable> items;
    private ArrayList<HashMap<String, Timetable>> reArrangedTime;
    private View listItemView;
    private int layout;

    public ListItemTimetableAdapter(Context context, ArrayList<Timetable>  items,
                                    int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
        reArrangedTime=getSubjectByDay(items);
    }


    @Override
    public int getCount() {
        return reArrangedTime.size();
    }

    @Override
    public Object getItem(int position) {
        return reArrangedTime.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        int height=CommonMethods.getDeviceWidth(context)/7;
        LinearLayout llItem=(LinearLayout)listItemView.findViewById(R.id.llItem);
        ViewGroup.LayoutParams params=llItem.getLayoutParams();
        params.height=height;
        llItem.setLayoutParams(params);

        TextView tvMon=(TextView)listItemView.findViewById(R.id.tvMon);
        setItemToView(tvMon,position);

        TextView tvTue=(TextView)listItemView.findViewById(R.id.tvTue);
        setItemToView(tvTue,position);

        TextView tvWed=(TextView)listItemView.findViewById(R.id.tvWed);
        setItemToView(tvWed,position);

        TextView tvThu=(TextView)listItemView.findViewById(R.id.tvThu);
        setItemToView(tvThu,position);

        TextView tvFri=(TextView)listItemView.findViewById(R.id.tvFri);
        setItemToView(tvFri,position);

        TextView tvSat=(TextView)listItemView.findViewById(R.id.tvSat);
        setItemToView(tvSat,position);

        TextView tvSun=(TextView)listItemView.findViewById(R.id.tvSun);
        setItemToView(tvSun,position);

        return listItemView;
    }

    private void setItemToView(TextView tvItem,int pos)
    {
        Timetable timetable=reArrangedTime.get(pos).get(tvItem.getText().toString());
        if(timetable!=null) {
            tvItem.setBackgroundColor(timetable.getSubject().getColor());
            tvItem.setText(timetable.getStartTime() + "\n" + timetable.getSubject().getShortName());
            tvItem.setTag(timetable);
            tvItem.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Timetable timetable=(Timetable) v.getTag();
                    final SelectEditorFragmentDialog selectEditorFragmentDialog=new SelectEditorFragmentDialog();
                    selectEditorFragmentDialog.setTimetable(timetable);
                    selectEditorFragmentDialog.setBtnDeleteClickListner(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            selectEditorFragmentDialog.dismiss();
                            final MessageFragmentDialog messageFragmentDialog = new MessageFragmentDialog();
                            messageFragmentDialog.setTitle("Delete Alert!");
                            messageFragmentDialog.setMessage("Are you sure you want to delete this item");
                            messageFragmentDialog.setBtnYesClickListner(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    messageFragmentDialog.dismiss();
                                    try {
                                        messageFragmentDialog.dismiss();
                                        CommonObjects.getDbHelper().deleteTimetableBySubjectId(timetable.getId());
                                        CommonMethods.showToast(Messages.TIMETABLE_DELETED_SUCCESSFULLY, Toast.LENGTH_LONG);
                                        items = updateList(items, timetable);
                                        reArrangedTime=getSubjectByDay(items);
                                        notifyDataSetChanged();
                                    } catch (Exception e) {
                                        CommonMethods.showToast(Messages.FAILED_TO_DELETE_TIMETABLE, Toast.LENGTH_LONG);
                                    }
                                }
                            });
                            messageFragmentDialog.show(((FragmentActivity) context).getSupportFragmentManager(), "");
                        }
                    });
                    selectEditorFragmentDialog.show(((FragmentActivity)context).getSupportFragmentManager(),"");
                }
            });
        }
        else
        {
            tvItem.setText("");
        }
    }

    private ArrayList<HashMap<String,Timetable>> getSubjectByDay(ArrayList<Timetable> timetables)
    {
        ArrayList<HashMap<String,Timetable>> items=new ArrayList<>();
        ArrayList<Timetable> mons=new ArrayList<>();
        ArrayList<Timetable> tues=new ArrayList<>();
        ArrayList<Timetable> weds=new ArrayList<>();
        ArrayList<Timetable> thus=new ArrayList<>();
        ArrayList<Timetable> fris=new ArrayList<>();
        ArrayList<Timetable> sats=new ArrayList<>();
        ArrayList<Timetable> suns=new ArrayList<>();

        for(Timetable timetable:timetables)
        {
            if(timetable.getDay().contains("Mon"))
            {
                mons.add(timetable);
            }
            if(timetable.getDay().contains("Tue"))
            {
                tues.add(timetable);
            }
            if(timetable.getDay().contains("Wed"))
            {
                weds.add(timetable);
            }
            if(timetable.getDay().contains("Thu"))
            {
                thus.add(timetable);
            }
            if(timetable.getDay().contains("Fri"))
            {
                fris.add(timetable);
            }
            if(timetable.getDay().contains("Sat"))
            {
                sats.add(timetable);
            }
            if(timetable.getDay().contains("Sun"))
            {
                suns.add(timetable);
            }
        }
        int count=mons.size();
        if(tues.size()>count)
        {
            count=tues.size();
        }
        if(weds.size()>count)
        {
            count=weds.size();
        }
        if(thus.size()>count)
        {
            count=thus.size();
        }
        if(fris.size()>count)
        {
            count=fris.size();
        }
        if(sats.size()>count)
        {
            count=sats.size();
        }
        if(suns.size()>count)
        {
            count=suns.size();
        }
        for(int i=0;i<count;i++)
        {
            HashMap<String,Timetable> maps=new HashMap<>();
            if(i<mons.size())
            {
                maps.put("Mon",mons.get(i));
            }
            if(i<tues.size())
            {
                maps.put("Tue",tues.get(i));
            }
            if(i<weds.size())
            {
                maps.put("Wed",weds.get(i));
            }
            if(i<thus.size())
            {
                maps.put("Thu",thus.get(i));
            }
            if(i<fris.size())
            {
                maps.put("Fri",fris.get(i));
            }
            if(i<sats.size())
            {
                maps.put("Sat",sats.get(i));
            }
            if(i<suns.size())
            {
                maps.put("Sun",suns.get(i));
            }
            items.add(maps);
        }
        return items;
    }


    private ArrayList<Timetable> updateList(ArrayList<Timetable> items,Timetable timetable)
    {
        ArrayList<Timetable> selectiveItems=new ArrayList<>();
        for(int i=0;i<items.size();i++)
        {
            if(items.get(i).getId()!=timetable.getId())
            {
                selectiveItems.add(items.get(i));
            }
        }
        return selectiveItems;
    }

}