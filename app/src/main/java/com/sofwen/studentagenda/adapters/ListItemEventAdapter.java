package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.acitvities.MainActivity;
import com.sofwen.studentagenda.business.objects.Event;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.fragments.UpdateEventsFragment;
import com.sofwen.studentagenda.fragments.UpdateSubjectFragment;
import com.sofwen.studentagenda.fragments.dialogs.MessageFragmentDialog;

import java.util.ArrayList;


public class ListItemEventAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Event> items;
    private View listItemView;
    private int layout;

    public ListItemEventAdapter(Context context, ArrayList<Event> items,
                                int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        TextView tvSubject = (TextView) listItemView.findViewById(R.id.tvSubject);
        tvSubject.setText(items.get(position).getSubjectId()+"");
        TextView tvName = (TextView) listItemView.findViewById(R.id.tvName);
        tvName.setText(items.get(position).getName());
        TextView tvDate = (TextView) listItemView.findViewById(R.id.tvDate);
        String string = items.get(position).getDate();
        String[] parts = string.split(" ");
        String part1 = parts[0]; // 004
        String part2 = parts[1]; // 034556
        String part3 = parts[2];
        tvDate.setText(part1 + " " + part2);
        TextView tvTime = (TextView) listItemView.findViewById(R.id.tvTime);
        tvTime.setText(items.get(position).getTime());
        Log.d("**time",items.get(position).getTime());
        final ImageButton ibDelete = (ImageButton) listItemView.findViewById(R.id.ibDelete);
        ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MessageFragmentDialog messageFragmentDialog = new MessageFragmentDialog();
                messageFragmentDialog.setTitle("Delete Alert!");
                messageFragmentDialog.setMessage("Are you sure you want to delete this item");
                messageFragmentDialog.setBtnYesClickListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            messageFragmentDialog.dismiss();
                            CommonObjects.getDbHelper().deleteEventById(items.get(position).getId());
                            CommonMethods.showToast(Messages.EVENT_DELETED_SUCCESSFULLY, Toast.LENGTH_LONG);
                            items = updateList(items, position);
                            notifyDataSetChanged();
                        } catch (Exception e) {
                            CommonMethods.showToast(Messages.FAILED_TO_DELETE_EVENT, Toast.LENGTH_LONG);
                        }
                    }
                });
                messageFragmentDialog.show(((FragmentActivity) context).getSupportFragmentManager(), "");
            }
        });
        LinearLayout llItem=(LinearLayout)listItemView.findViewById(R.id.llItem);
        llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateEventsFragment updateEventsFragment=UpdateEventsFragment.createFragment();
                updateEventsFragment.setEvent(items.get(position));
                CommonMethods.callFragment(updateEventsFragment, R.id.flMenuFragments, R.anim.from_right, R.anim.zoom_out, R.anim.zoom_in, R.anim.to_right, context, UpdateEventsFragment.TAG);
            }
        });
        return listItemView;
    }

    private ArrayList<Event> updateList(ArrayList<Event> items,int pos)
    {
        ArrayList<Event> selectiveItems=new ArrayList<>();
        for(int i=0;i<items.size();i++)
        {
            if(i!=pos)
            {
                selectiveItems.add(items.get(i));
            }
        }
        return selectiveItems;
    }

}