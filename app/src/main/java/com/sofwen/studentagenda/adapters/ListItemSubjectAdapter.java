package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.fragments.AddSubjectFragment;
import com.sofwen.studentagenda.fragments.UpdateSubjectFragment;
import com.sofwen.studentagenda.fragments.dialogs.MessageFragmentDialog;

import java.util.ArrayList;


public class ListItemSubjectAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Subject> items;
    private View listItemView;
    private int layout;

    public ListItemSubjectAdapter(Context context, ArrayList<Subject> items,
                                  int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        TextView tvShortName = (TextView) listItemView.findViewById(R.id.tvShortName);
        tvShortName.setText(items.get(position).getShortName());
        TextView tvName = (TextView) listItemView.findViewById(R.id.tvName);
        tvName.setText(items.get(position).getName());
        TextView tvTeacherName = (TextView) listItemView.findViewById(R.id.tvTeacherName);
        tvTeacherName.setText(items.get(position).getTeacherName());
        final ImageButton ibDelete = (ImageButton) listItemView.findViewById(R.id.ibDelete);
        ibDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MessageFragmentDialog messageFragmentDialog = new MessageFragmentDialog();
                messageFragmentDialog.setTitle("Delete Alert!");
                messageFragmentDialog.setMessage("Are you sure you want to delete this item");
                messageFragmentDialog.setBtnYesClickListner(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            messageFragmentDialog.dismiss();
                            CommonObjects.getDbHelper().deleteSubjectById(items.get(position).getId());
                            CommonObjects.getDbHelper().deleteTimetableBySubjectId(items.get(position).getId());
                            CommonMethods.showToast(Messages.SUBJECT_DELETED_SUCCESSFULLY, Toast.LENGTH_LONG);
                            items = updateList(items, position);
                            notifyDataSetChanged();
                        } catch (Exception e) {
                            CommonMethods.showToast(Messages.FAILED_TO_DELETE_SUBJECT, Toast.LENGTH_LONG);
                        }
                    }
                });
                messageFragmentDialog.show(((FragmentActivity) context).getSupportFragmentManager(), "");
            }
        });
        LinearLayout llItem=(LinearLayout)listItemView.findViewById(R.id.llItem);
        llItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UpdateSubjectFragment updateSubjectFragment=UpdateSubjectFragment.createFragment();
                updateSubjectFragment.setSubject(items.get(position));
                CommonMethods.callFragment(updateSubjectFragment,R.id.flMenuFragments,R.anim.from_right,R.anim.zoom_out,R.anim.zoom_in,R.anim.to_right,context,UpdateSubjectFragment.TAG);
            }
        });
        ImageView ivColor=(ImageView)listItemView.findViewById(R.id.ivColor);
        ivColor.setBackgroundColor(items.get(position).getColor());
        return listItemView;
    }

    private ArrayList<Subject> updateList(ArrayList<Subject> items,int pos)
    {
        ArrayList<Subject> selectiveItems=new ArrayList<>();
        for(int i=0;i<items.size();i++)
        {
            if(i!=pos)
            {
                selectiveItems.add(items.get(i));
            }
        }
        return selectiveItems;
    }

}