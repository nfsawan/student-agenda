package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.Utility.Messages;
import com.sofwen.studentagenda.business.objects.Subject;
import com.sofwen.studentagenda.fragments.UpdateSubjectFragment;
import com.sofwen.studentagenda.fragments.dialogs.MessageFragmentDialog;

import java.util.ArrayList;


public class ListItemSelectSubjectAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Subject> items;
    private View listItemView;
    private int layout;

    public ListItemSelectSubjectAdapter(Context context, ArrayList<Subject> items,
                                        int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        TextView tvShortName = (TextView) listItemView.findViewById(R.id.tvShortName);
        tvShortName.setText(items.get(position).getShortName());
        return listItemView;
    }

}