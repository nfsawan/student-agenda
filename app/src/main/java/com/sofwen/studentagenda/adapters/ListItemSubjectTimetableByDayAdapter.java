package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.business.objects.Timetable;
import java.util.ArrayList;


public class ListItemSubjectTimetableByDayAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Timetable> items;
    private View listItemView;
    private int layout;

    public ListItemSubjectTimetableByDayAdapter(Context context, ArrayList<Timetable> items,
                                                int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
    }


    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        TextView tvSubject = (TextView) listItemView.findViewById(R.id.tvSubject);
        tvSubject.setText(items.get(position).getSubject().getShortName());
        ImageView ivColor=(ImageView)listItemView.findViewById(R.id.ivColor);
        ivColor.setBackgroundColor(items.get(position).getSubject().getColor());
        TextView tvStartTime = (TextView) listItemView.findViewById(R.id.tvStartTime);
        tvStartTime.setText(items.get(position).getStartTime());
        TextView tvEndTime = (TextView) listItemView.findViewById(R.id.tvEndTime);
        tvEndTime.setText(items.get(position).getStartTime());
        return listItemView;
    }

}