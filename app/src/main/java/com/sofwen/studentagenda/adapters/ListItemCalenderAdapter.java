package com.sofwen.studentagenda.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.business.objects.Date;

import java.util.ArrayList;
import java.util.HashMap;


public class ListItemCalenderAdapter extends BaseAdapter{
    private Context context;
    private ArrayList<Date> items;
    private ArrayList<HashMap<String, Date>> reArrangedTime;
    private View listItemView;
    private int layout;

    public ListItemCalenderAdapter(Context context, ArrayList<Date>  items,
                                   int layout) {
        this.context = context;
        this.items = items;
        this.layout = layout;
        reArrangedTime=getCalByDay(items);
    }


    @Override
    public int getCount() {
        return reArrangedTime.size();
    }

    @Override
    public Object getItem(int position) {
        return reArrangedTime.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        listItemView = CommonMethods.createView(context, layout, parent);
        int height= CommonMethods.getDeviceWidth(context)/7;
        LinearLayout llItem=(LinearLayout)listItemView.findViewById(R.id.llItem);
        ViewGroup.LayoutParams params=llItem.getLayoutParams();
        params.height=height;
        llItem.setLayoutParams(params);

        TextView tvMon=(TextView)listItemView.findViewById(R.id.tvMon);
        setItemToView(tvMon,position);

        TextView tvTue=(TextView)listItemView.findViewById(R.id.tvTue);
        setItemToView(tvTue,position);

        TextView tvWed=(TextView)listItemView.findViewById(R.id.tvWed);
        setItemToView(tvWed,position);

        TextView tvThu=(TextView)listItemView.findViewById(R.id.tvThu);
        setItemToView(tvThu,position);

        TextView tvFri=(TextView)listItemView.findViewById(R.id.tvFri);
        setItemToView(tvFri,position);

        TextView tvSat=(TextView)listItemView.findViewById(R.id.tvSat);
        setItemToView(tvSat,position);

        TextView tvSun=(TextView)listItemView.findViewById(R.id.tvSun);
        setItemToView(tvSun,position);

        return listItemView;
    }

    private void setItemToView(TextView tvItem,int pos)
    {
        Date date=reArrangedTime.get(pos).get(tvItem.getText().toString());
        if(date!=null) {
            tvItem.setText(date.getDayNumber()+"");
        }
        else
        {
            tvItem.setText("");
        }
    }
    
    private ArrayList<HashMap<String,Date>> getCalByDay(ArrayList<Date> dates) {
        ArrayList<HashMap<String, Date>> items = new ArrayList<>();
        HashMap<String,Date> row1=new HashMap<>();
        for(Date date:dates)
        {
            if(row1.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()<2)
            {
                row1.put("Mon",date);
            }
            if(row1.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()<3)
            {
                row1.put("Tue",date);
            }
            if(row1.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()<4)
            {
                row1.put("Wed",date);
            }
            if(row1.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()<5)
            {
                row1.put("Thu",date);
            }
            if(row1.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()<6)
            {
                row1.put("Fri",date);
            }
            if(row1.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()<7)
            {
                row1.put("Sat",date);
            }
            if(row1.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()<8)
            {
                row1.put("Sun",date);
            }
        }
        HashMap<String,Date> row2=new HashMap<>();
        for(Date date:dates)
        {
            if(row2.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()>1&&date.getDayNumber()<9)
            {
                row2.put("Mon",date);
            }
            if(row2.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()>2&&date.getDayNumber()<10)
            {
                row2.put("Tue",date);
            }
            if(row2.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()>3&&date.getDayNumber()<11)
            {
                row2.put("Wed",date);
            }
            if(row2.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()>4&&date.getDayNumber()<12)
            {
                row2.put("Thu",date);
            }
            if(row2.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()>5&&date.getDayNumber()<13)
            {
                row2.put("Fri",date);
            }
            if(row2.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()>6&&date.getDayNumber()<14)
            {
                row2.put("Sat",date);
            }
            if(row2.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()>7&&date.getDayNumber()<15)
            {
                row2.put("Sun",date);
            }
        }
        HashMap<String,Date> row3=new HashMap<>();
        for(Date date:dates)
        {
            if(row3.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()>8&&date.getDayNumber()<16)
            {
                row3.put("Mon",date);
            }
            if(row3.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()>9&&date.getDayNumber()<17)
            {
                row3.put("Tue",date);
            }
            if(row3.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()>10&&date.getDayNumber()<18)
            {
                row3.put("Wed",date);
            }
            if(row3.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()>11&&date.getDayNumber()<19)
            {
                row3.put("Thu",date);
            }
            if(row3.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()>12&&date.getDayNumber()<20)
            {
                row3.put("Fri",date);
            }
            if(row3.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()>13&&date.getDayNumber()<21)
            {
                row3.put("Sat",date);
            }
            if(row3.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()>14&&date.getDayNumber()<22)
            {
                row3.put("Sun",date);
            }
        }    
        HashMap<String,Date> row4=new HashMap<>();
        for(Date date:dates)
        {
            if(row4.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()>15&&date.getDayNumber()<23)
            {
                row4.put("Mon",date);
            }
            if(row4.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()>16&&date.getDayNumber()<24)
            {
                row4.put("Tue",date);
            }
            if(row4.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()>17&&date.getDayNumber()<25)
            {
                row4.put("Wed",date);
            }
            if(row4.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()>18&&date.getDayNumber()<26)
            {
                row4.put("Thu",date);
            }
            if(row4.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()>19&&date.getDayNumber()<27)
            {
                row4.put("Fri",date);
            }
            if(row4.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()>20&&date.getDayNumber()<28)
            {
                row4.put("Sat",date);
            }
            if(row4.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()>21&&date.getDayNumber()<29)
            {
                row4.put("Sun",date);
            }
        }
        HashMap<String,Date> row5=new HashMap<>();
        for(Date date:dates)
        {
            if(row5.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()>22&&date.getDayNumber()<30)
            {
                row5.put("Mon",date);
            }
            if(row5.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()>23&&date.getDayNumber()<31)
            {
                row5.put("Tue",date);
            }
            if(row5.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()>24&&date.getDayNumber()<32)
            {
                row5.put("Wed",date);
            }
            if(row5.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()>25&&date.getDayNumber()<33)
            {
                row5.put("Thu",date);
            }
            if(row5.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()>26&&date.getDayNumber()<34)
            {
                row5.put("Fri",date);
            }
            if(row5.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()>27&&date.getDayNumber()<35)
            {
                row5.put("Sat",date);
            }
            if(row5.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()>28&&date.getDayNumber()<36)
            {
                row5.put("Sun",date);
            }
        }
        HashMap<String,Date> row6=new HashMap<>();
        for(Date date:dates)
        {
            if(row6.get("Mon")==null&&date.getDay().equals("Mon")&&date.getDayNumber()>29&&date.getDayNumber()<37)
            {
                row6.put("Mon",date);
            }
            if(row6.get("Tue")==null&&date.getDay().equals("Tue")&&date.getDayNumber()>30&&date.getDayNumber()<38)
            {
                row6.put("Tue",date);
            }
            if(row6.get("Wed")==null&&date.getDay().equals("Wed")&&date.getDayNumber()>31&&date.getDayNumber()<39)
            {
                row6.put("Wed",date);
            }
            if(row6.get("Thu")==null&&date.getDay().equals("Thu")&&date.getDayNumber()>32&&date.getDayNumber()<40)
            {
                row6.put("Thu",date);
            }
            if(row6.get("Fri")==null&&date.getDay().equals("Fri")&&date.getDayNumber()>33&&date.getDayNumber()<41)
            {
                row6.put("Fri",date);
            }
            if(row6.get("Sat")==null&&date.getDay().equals("Sat")&&date.getDayNumber()>34&&date.getDayNumber()<42)
            {
                row6.put("Sat",date);
            }
            if(row6.get("Sun")==null&&date.getDay().equals("Sun")&&date.getDayNumber()>35&&date.getDayNumber()<43)
            {
                row6.put("Sun",date);
            }
        }
        items.add(row1);
        items.add(row2);
        items.add(row3);
        items.add(row4);
        items.add(row5);
        items.add(row6);
        return items;
    }

}