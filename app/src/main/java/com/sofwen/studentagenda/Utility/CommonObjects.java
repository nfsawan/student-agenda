package com.sofwen.studentagenda.Utility;

import android.content.Context;

import com.sofwen.studentagenda.database.DBHelper;


public class CommonObjects {
    private static Context context;
    private static DBHelper dbHelper;

    public static Context getContext() {
        return context;
    }

    public static void setContext(Context context) {
        CommonObjects.context = context;
    }

    public static DBHelper getDbHelper() {
        if(dbHelper==null)
        dbHelper=new DBHelper(context);
        return dbHelper;
    }

}
