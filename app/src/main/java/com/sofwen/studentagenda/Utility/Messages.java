package com.sofwen.studentagenda.Utility;


public class Messages {
    public static String SUBJECT_SHORT_NAME_REQUIRED="Please enter short name for subject";
    public static String EVENT_NAME_REQUIRED="Please enter event name for subject";

    public static String FAILED_TO_ADD_SUBJECT="Failed to add new subject";
    public static String FAILED_TO_ADD_MARKS="Failed to add new marks";

    public static String FAILED_TO_ADD_EVENT="Failed to add new event";
    public static String NEW_SUBJECT_ADDED_SUCCESSFULLY="New subject added successfully";
    public static String FAILED_TO_DELETE_SUBJECT="Failed to delete subject";
    public static String FAILED_TO_DELETE_EVENT="Failed to delete event";
    public static String FAILED_TO_DELETE_TIMETABLE="Failed to delete timetable";
    public static String NEW_MARKS_ADDED_SUCCESSFULLY="New marks added successfully";

    public static String SUBJECT_DELETED_SUCCESSFULLY="Subject deleted successfully";
    public static String EVENT_DELETED_SUCCESSFULLY="Event deleted successfully";
    public static String TIMETABLE_DELETED_SUCCESSFULLY="Timetable deleted successfully";

    public static String FAILED_TO_LOAD_SUBJECT="Failed to load subjects";
    public static String FAILED_TO_LOAD_EVENT="Failed to load events";
    public static String FAILED_TO_LOAD_TIMETABLE="Failed to load timetables";
    public static String FAILED_TO_LOAD_APP_COLORS="Failed to load app colors";

    public static String FAILED_TO_UPDATE_SUBJECT="Failed to update subject";
    public static String SUBJECT_UPDATED_SUCCESSFULLY="Subject updated successfully";
    public static String FAILED_TO_UPDATE_APP_COLORS="Failed to update app colors";
    public static String APP_COLORS_UPDATED_SUCCESSFULLY="App colors updated successfully";
    public static String EVENT_ADDED_SUCCESSFULLY="Event Added successfully";
    public static String PLEASE_ADD_A_SUBJECT_FIRST="Please add a subject first";
    public static String DAY_REQUIRED="Please select day of week";
    public static String SUBJECT_REQUIRED="Please select a subject";
    public static String START_TIME_REQUIRED="Please select start time";
    public static String FAILED_TO_ADD_TIMETABLE="Failed to add new tmetable";
    public static String NEW_TIMETABLE_ADDED_SUCCESSFULLY="New timetable added successfully";
    public static String FAILED_TO_UPDATE_TIMETABLE="Failed to update tmetable";
    public static String TIMETABLE_UPDATED_SUCCESSFULLY="Timetable updated successfully";
    public static String MARKS_REQUIRED="Marks Required";
}
