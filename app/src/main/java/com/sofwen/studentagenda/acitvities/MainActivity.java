package com.sofwen.studentagenda.acitvities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;

import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItems;
import com.sofwen.studentagenda.R;
import com.sofwen.studentagenda.Utility.CommonMethods;
import com.sofwen.studentagenda.Utility.CommonObjects;
import com.sofwen.studentagenda.business.objects.Color;
import com.sofwen.studentagenda.fragments.CalenderFragment;
import com.sofwen.studentagenda.fragments.DayFragment;
import com.sofwen.studentagenda.fragments.MenuFragment;
import com.sofwen.studentagenda.fragments.WeekFragment;

public class MainActivity extends FragmentActivity {
    private ViewPager viewPager;
    private FragmentPagerItemAdapter mainAdapter;
    private int currentPos=0;

    public FragmentPagerItemAdapter getMainAdapter()
    {
        return mainAdapter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CommonObjects.setContext(getApplicationContext());

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        if (fab != null) {
//            fab.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                            .setAction("Action", null).show();
//                }
//            });
//        }
        mainAdapter = new FragmentPagerItemAdapter(
                getSupportFragmentManager(), FragmentPagerItems.with(this)
                .add(R.string.titleA, MenuFragment.class)
                .add(R.string.titleB, DayFragment.class)
                .add(R.string.titleC, WeekFragment.class)
                .add(R.string.titleD, CalenderFragment.class)
                .create());

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setAdapter(mainAdapter);

        SmartTabLayout viewPagerTab = (SmartTabLayout) findViewById(R.id.viewpagertab);
//        viewPagerTab.setCustomTabView(R.layout.custom_tab,R.id.custom_tab);
        viewPagerTab.setViewPager(viewPager);
        CommonMethods.setupUI(findViewById(R.id.parent), this);
        addDefaultColor();
    }

    private void addDefaultColor()
    {
        try {
            if (CommonObjects.getDbHelper().getColors().size() == 0)
            {
                Color color=new Color();
                color.setBackgroundColor(-11419154);
                color.setExamColor(-4224594);
                color.setActivityColor(-740056);
                color.setCourseworkHomeworkColor(-10965321);
                color.setReturnBookColor(-14654801);
                CommonObjects.getDbHelper().insertColor(color);
            }
        } catch (Exception e) {
        }
    }

    public interface OnUpdateNotifier
    {
        public void onUpdate();
    }
}
