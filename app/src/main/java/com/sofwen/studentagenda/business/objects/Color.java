package com.sofwen.studentagenda.business.objects;


public class Color {
    private int id;
    private int backgroundColor;
    private int examColor;
    private int courseworkHomeworkColor;
    private int activityColor;
    private int returnBookColor;

    public int getActivityColor() {
        return activityColor;
    }

    public void setActivityColor(int activityColor) {
        this.activityColor = activityColor;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public int getCourseworkHomeworkColor() {
        return courseworkHomeworkColor;
    }

    public void setCourseworkHomeworkColor(int courseworkHomeworkColor) {
        this.courseworkHomeworkColor = courseworkHomeworkColor;
    }

    public int getExamColor() {
        return examColor;
    }

    public void setExamColor(int examColor) {
        this.examColor = examColor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getReturnBookColor() {
        return returnBookColor;
    }

    public void setReturnBookColor(int returnBookColor) {
        this.returnBookColor = returnBookColor;
    }
}